def GIT_REPOS_URL = "<YOUR REPO URL>"
def GIT_BRANCH = "<YOUR BRANCH NAME>"
def GITLAB_CREDENTIAL = "YOUR GITLAB CREDENCIALS CONFIGURED ON JENKINS"
def WIN64 = "<NAME OF YOUR JENKINS NODE SLAVE WIN64>"
def WIN86 = "<NAME OF YOUR JENKINS NODE SLAVE WIN86>"


pipeline {
    agent none  
    stages {
        stage ("FTP RESULTS") {
            parallel {
                stage ("Upload Results [Win64]"){
                    agent {
                        label WIN64
                    }
                    
                    steps {
                        dir ("${RELEASE_RESULTS}"){
                            ftpPublisher alwaysPublishFromMaster: false,
                            continueOnError: false,
                            failOnError: false,
                            masterNodeName: '',
                            paramPublish: null,
                            publishers: [[
                                configName: '<FTP CREDENTIALS CONFIG>', 
                                transfers: [[
                                    asciiMode: false, 
                                    cleanRemote: false, 
                                    excludes: '', 
                                    flatten: false, 
                                    makeEmptyDirs: true, 
                                    noDefaultExcludes: false, 
                                    patternSeparator: '[, ]+', 
                                    remoteDirectory: "<REMOTE PATH ON FTP SERVER>/${RELEASE_NAME}", 
                                    remoteDirectorySDF: false, 
                                    removePrefix: '', 
                                    sourceFiles: '**/*'
                                ]], 
                            usePromotionTimestamp: false, 
                            useWorkspaceInPromotion: false, 
                            verbose: true
                            ]]
                        }    
                    }
                }
                stage ("Upload Results [Win86]"){
                    agent {
                        label WIN86
                    }
                    
                    steps {
                        dir ("${RELEASE_RESULTS}"){
                            ftpPublisher alwaysPublishFromMaster: false,
                            continueOnError: false,
                            failOnError: false,
                            masterNodeName: '',
                            paramPublish: null,
                            publishers: [[
                                configName: '<FTP CREDENTIALS CONFIG>', 
                                transfers: [[
                                    asciiMode: false, 
                                    cleanRemote: false, 
                                    excludes: '', 
                                    flatten: false, 
                                    makeEmptyDirs: true, 
                                    noDefaultExcludes: false, 
                                    patternSeparator: '[, ]+', 
                                    remoteDirectory: "<REMOTE PATH ON FTP SERVER>/${RELEASE_NAME}", 
                                    remoteDirectorySDF: false, 
                                    removePrefix: '', 
                                    sourceFiles: '**/*'
                                ]], 
                            usePromotionTimestamp: false, 
                            useWorkspaceInPromotion: false, 
                            verbose: true
                            ]]
                        }    
                    }
                }
            }
        }
    }
}